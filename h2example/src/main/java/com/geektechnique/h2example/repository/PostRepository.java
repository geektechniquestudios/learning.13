package com.geektechnique.h2example.repository;

import com.geektechnique.h2example.domain.Post;
import org.springframework.data.repository.CrudRepository;

public interface PostRepository extends CrudRepository<Post, Long> { //<classType, id>
}
