package com.geektechnique.h2example;

import com.geektechnique.h2example.domain.Post;
import com.geektechnique.h2example.repository.PostRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class H2exampleApplication {

	@Autowired
	PostRepository postRepository;

	private static final Logger logger = LoggerFactory.getLogger(H2exampleApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(H2exampleApplication.class, args);
	}

	@PostConstruct//lets us run method after everything is made - atypical
	void seePosts(){
		logger.info("this shows what post method is called");
		for(Post post : postRepository.findAll()){
			logger.info(post.toString());
		}
	}
}
